from core.models import GeneralParameter


def export_vars(request):
    return {'general_parameter': GeneralParameter.load()}
