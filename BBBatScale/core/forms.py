from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Submit, Div, Fieldset, HTML
from django.utils.translation import ugettext_lazy as _
from .models import Instance, Room, RoomConfiguration, Tenant, GeneralParameter
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import PasswordChangeForm


class GeneralParametersForm(forms.ModelForm):
    class Meta:
        model = GeneralParameter
        fields = ['latest_news', 'jitsi_url', 'faq_url', 'feedback_email', 'footer',
                  'logo_link', 'favicon_link', 'page_title', 'home_room_enabled',
                  'playback_url', 'home_room_teachers_only', 'home_room_tenant',
                  'home_room_room_configuration']

    def __init__(self, *args, **kwargs):
        super(GeneralParametersForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-3'
        self.helper.field_class = 'col-lg-9'
        self.helper.layout = Layout(
            Field("latest_news"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                '<h4>{}</h4></div></div>'.format(_('General site settings'))),
            Field("page_title"),
            Field("logo_link"),
            Field("favicon_link"),
            Field("jitsi_url"),
            Field("faq_url"),
            Field("footer"),
            HTML(
                '<hr><div class="row mb-5"><div class="col-lg-3"></div><div class="col-lg-9">'
                '<h4>{}</h4></div></div>'.format(_('Feature: Home room'))),
            Field("home_room_enabled"),
            Field("home_room_teachers_only"),
            Field("home_room_tenant"),
            Field("home_room_room_configuration"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class InstanceForm(forms.ModelForm):
    class Meta:
        model = Instance
        fields = ['dns', 'datacenter', 'shared_secret', 'tenant', 'participant_count_max', 'videostream_count_max']

    def __init__(self, *args, **kwargs):
        super(InstanceForm, self).__init__(*args, **kwargs)

        self.fields['dns'].label = _("DNS")
        self.fields['tenant'].label = _("Tenant")
        self.fields['datacenter'].label = _("Datacenter")
        self.fields['shared_secret'].label = _("Shared Secret")
        self.fields['shared_secret'].required = True
        self.fields['participant_count_max'].label = _("Max participants")
        self.fields['videostream_count_max'].label = _("Max videostreams")

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout = Layout(
            Field("dns"),
            Field("tenant"),
            Field("datacenter"),
            Field("shared_secret"),
            Field("participant_count_max"),
            Field("videostream_count_max"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class RoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = ['tenant', 'name', 'config', "is_public", "comment_private", "comment_public"]

    def __init__(self, *args, **kwargs):
        super(RoomForm, self).__init__(*args, **kwargs)

        self.fields['tenant'].label = _("Tenant")
        self.fields['name'].label = _("Name")
        self.fields['config'].label = _("Configuration")
        self.fields['is_public'].label = _("Public")
        self.fields['comment_private'].label = _("Comment private")
        self.fields['comment_public'].label = _("Comment public")

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout = Layout(
            Field("tenant"),
            Field("name"),
            Field("config"),
            Field("is_public"),
            Field("comment_private"),
            Field("comment_public"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class RoomConfigurationForm(forms.ModelForm):
    class Meta:
        model = RoomConfiguration
        fields = ["name", "mute_on_start", "all_moderator", "everyone_can_start", "allow_guest_entry",
                  "access_code", "access_code_guests", "disable_mic", "disable_cam", "allow_recording", "disable_note",
                  "disable_private_chat", "disable_public_chat", "authenticated_user_can_start", "guest_policy"]

    def __init__(self, *args, **kwargs):
        super(RoomConfigurationForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = _("Name")
        self.fields['mute_on_start'].label = _("Everyone is muted on start")
        self.fields['all_moderator'].label = _("Everyone is moderator")
        self.fields['everyone_can_start'].label = _("Everyone can start meeting")
        self.fields['authenticated_user_can_start'].label = _("Registered user can start meeting")
        self.fields['allow_guest_entry'].label = _("Allow guests")
        self.fields['access_code_guests'].label = " "
        self.fields['access_code'].label = _("Access code")
        self.fields['disable_cam'].label = _("Disable camera for non moderators")
        self.fields['disable_mic'].label = _("Disable microphone for non moderators")
        self.fields['allow_recording'].label = _("Allow recording")
        self.fields['disable_note'].label = _("Disable shared notes - editing only for moderators")
        self.fields['disable_public_chat'].label = _("Disable public chat - editing only for moderators")
        self.fields['disable_private_chat'].label = _("Disable private chat - editing only for moderators")
        self.fields['guest_policy'].label = _("Enable Guestlobby")

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-4'
        self.helper.layout = Layout(
            Field("name"),
            Field("mute_on_start"),
            Field("all_moderator"),
            Field("everyone_can_start"),
            Field("authenticated_user_can_start"),
            Field("disable_cam"),
            Field("disable_mic"),
            Field("disable_note"),
            Field("disable_public_chat"),
            Field("disable_private_chat"),
            Field("allow_recording"),
            Field("allow_guest_entry"),
            Field("access_code", placeholder="For everyone"),
            Field("access_code_guests", placeholder="Only for guests"),
            Field("guest_policy"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class TenantForm(forms.ModelForm):
    class Meta:
        model = Tenant
        fields = ["name", "scheduling_strategy", "description", "notifications_emails"]

    def __init__(self, *args, **kwargs):
        super(TenantForm, self).__init__(*args, **kwargs)

        self.fields['name'].label = _("Name")
        self.fields['scheduling_strategy'].label = _("Scheduling Strategy")
        self.fields['description'].label = _("Description")
        self.fields['notifications_emails'].label = _("Email Notifications")

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout = Layout(
            Field("name"),
            Field("scheduling_strategy"),
            Field("description"),
            Field("notifications_emails"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class ConfigureRoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = ["mute_on_start", "all_moderator", "allow_guest_entry",
                  "access_code", "access_code_guests", "disable_mic", "disable_cam", "allow_recording", "disable_note",
                  "disable_private_chat", "disable_public_chat", "guest_policy"]

    def __init__(self, *args, **kwargs):
        super(ConfigureRoomForm, self).__init__(*args, **kwargs)

        self.fields['mute_on_start'].label = _("Everyone is muted on start")
        self.fields['all_moderator'].label = _("Everyone is moderator")
        self.fields['allow_guest_entry'].label = _("Allow guests")
        self.fields['access_code_guests'].label = " "
        self.fields['access_code'].label = _("Access code")
        self.fields['disable_cam'].label = _("Disable camera for non-moderators")
        self.fields['disable_mic'].label = _("Disable microphone for non-moderators")
        self.fields['allow_recording'].label = _("Allow recording")
        self.fields['disable_note'].label = _("Disable shared notes - editing only for moderators")
        self.fields['disable_public_chat'].label = _("Disable public chat - editing only for moderators")
        self.fields['disable_private_chat'].label = _("Disable private chat - editing only for moderators")
        self.fields['guest_policy'].label = _("Enable Guestlobby")

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-4'
        self.helper.layout = Layout(
            Field("mute_on_start"),
            Field("all_moderator"),
            Field("disable_cam"),
            Field("disable_mic"),
            Field("disable_note"),
            Field("disable_public_chat"),
            Field("disable_private_chat"),
            Field("allow_recording"),
            Field("allow_guest_entry"),
            Field("access_code", placeholder="For everyone"),
            Field("access_code_guests", placeholder="Only for guests"),
            Field("guest_policy"),
            Submit("save", _("Start"), css_class="btn-primary"),
        )


class RoomFilterFormHelper(FormHelper):
    _form_method = 'GET'
    layout = Layout(
        Fieldset("", Div(
            Div("name", css_class="col"),
            Div("tenant", css_class="col"),
            Div("instance", css_class="col"),
            Div("config", css_class="col"),
            css_class="form-row",
        )),
        Submit('submit', _("Filter"), css_class="btn-primary btn-small"),

    )


class InstanceFilterFormHelper(FormHelper):
    _form_method = 'GET'
    layout = Layout(
        Fieldset("", Div(
            Div("tenant", css_class="col"),
            Div("dns", css_class="col"),
            Div("state", css_class="col"),
            css_class="form-row",
        )),
        Submit('submit', _("Filter"), css_class="btn-primary btn-small"),

    )


class TenantFilterFormHelper(FormHelper):
    _form_method = 'GET'
    layout = Layout(
        Fieldset("", Div(
            Div("name", css_class="col"),
            css_class="form-row",
        )),
        Submit('submit', _("Filter"), css_class="btn-primary btn-small"),

    )


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'groups', 'is_staff', 'password']

    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        self.fields['username'].label = _("Username")
        self.fields['first_name'].label = _("First name")
        self.fields['last_name'].label = _("Last name")
        self.fields['groups'].label = _("Groups")
        self.fields['is_staff'].label = _("Staff status")
        self.fields['password'].label = _("Password")

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout = Layout(
            Field("username"),
            Field("password"),
            Field("first_name"),
            Field("last_name"),
            Field("groups"),
            Field("is_staff"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'groups', 'is_staff']

    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)

        self.fields['username'].label = _("Username")
        self.fields['first_name'].label = _("First name")
        self.fields['last_name'].label = _("Last name")
        self.fields['groups'].label = _("Groups")
        self.fields['is_staff'].label = _("Staff status")

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout = Layout(
            Field("username"),
            Field("first_name"),
            Field("last_name"),
            Field("groups"),
            Field("is_staff"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )


class PasswordChangeCrispyForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(PasswordChangeCrispyForm, self).__init__(*args, **kwargs)
        self.fields['old_password'].label = _("Old password")
        self.fields['new_password1'].label = _("New password")
        self.fields['new_password2'].label = _("Repeat new password")

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-4'
        self.helper.field_class = 'col-lg-8'
        self.helper.layout = Layout(
            Field("old_password"),
            Field("new_password1"),
            Field("new_password2"),
            Submit("save", _("Save"), css_class="btn-primary"),
        )
